package Apple;

import core.BaseTest;
import io.qameta.allure.Description;
import io.qameta.allure.Owner;
import org.junit.Assert;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
@Tag("API")
//Для скиншота при ошибке
// @ExtendWith(TestListener.class)
public class AppleTest extends BaseTest {
    private final static String BASE_URL = "https://appleinsider.ru/";
    private final static String SEARCH_STRING = "Чем iPhone 13  отличается от iPhone 12";
    private final static String EXPECTED_WORD = "iphone-13";

    @Test
    @Owner("MivchenkoAV")
    @Description("ищем айфон")
    public void checkHref (){
        Assert.assertTrue(new MainPage(BASE_URL)
                .search(SEARCH_STRING)
                .getHrefFromFirstArticle()
                .contains(EXPECTED_WORD));
    }
}
