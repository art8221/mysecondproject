package core;


import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.logevents.SelenideLogger;
import io.github.bonigarcia.wdm.WebDriverManager;
import io.qameta.allure.selenide.AllureSelenide;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;


abstract public class BaseTest {//Базовый абстрактный класс(от которого буду наследоваться в других тестовых классах)
    public void setUp() {//настройки для браузера
        WebDriverManager.chromedriver().setup();
        Configuration.browser  = "chrome";
        Configuration.driverManagerEnabled = true;//указываю что присутствует менеджер
        Configuration.browserSize = "1366x768";
        Configuration.headless = true;//буду ли я видеть сам браузер при выполнении тестов(false -видно)
        Configuration.timeout= 5000;
        SelenideLogger.addListener("AllureSelenide" , new AllureSelenide());//Для логирования силенида


    }
    @BeforeEach//Метод ДО теста
    public void init(){
        setUp();
    }
   @AfterEach//Метод ПОСЛЕ теста
    public void tearDown (){
        Selenide.closeWebDriver();
    }
}


