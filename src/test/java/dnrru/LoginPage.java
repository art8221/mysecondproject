package dnrru;

import com.codeborne.selenide.Selenide;
import org.junit.Assert;

import static com.codeborne.selenide.Selenide.$x;
import static com.codeborne.selenide.Selenide.title;


public class LoginPage {



    void enterUserName(String login){
        Selenide.$x("//input[@id=\"login_modal\"]").setValue(login);
    }
    void enterPassword(String pass){
        Selenide.$x("//input[@id=\"pass_modal\"]").setValue(pass);
    }
    void clickEntry(){
        Selenide.$x("//span[text()=\"Вход\"]").click();
    }
    void clickLogin(){
        Selenide.$x("//input[@id=\"go_modal\"]").click();
    }
    void open(String URL){
         Selenide.open(URL);
    }
    boolean atPage(){
         if ((title()).equals("Интернет-магазин автозапчастей Донецк, ДНР - DN-R.RU")){
             return true;
         }
         else return false;
    }
    boolean invalidNamePass(){
        if ($x("//div[@class=\"error fr-text-danger\"]")
                .getText().equals("Неверный логин/пароль!")){
            return true;
        }
        else return false;
    }
    void test(){
        Assert.assertEquals("Неверный логин/пароль !",
                $x("//div[@class=\"error fr-text-danger\"]").getText());
    }
}
