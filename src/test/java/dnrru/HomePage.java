package dnrru;

import static com.codeborne.selenide.Selenide.$x;
import static com.codeborne.selenide.Selenide.title;

public class HomePage {
    boolean atPage(){
        if ((title()).equals("Интернет-магазин автозапчастей Донецк, ДНР - DN-R.RU")){
            return true;
        }
        else return false;
    }
    boolean loggedIn(){
        if ($x("//span[@class=\"clientName\"]")
                 .getText().replaceAll("\\D+" , "").equals("6051079")){
            return true;
        }
        else return false;
    }
    void logOut(){
        $x("//a[@class=\"exitIcon\"]").click();
    }


}