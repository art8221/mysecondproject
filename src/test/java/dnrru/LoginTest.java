package dnrru;

import core.BaseTest;
import io.qameta.allure.Description;
import io.qameta.allure.Owner;
import org.apiguardian.api.API;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import static org.junit.Assert.assertTrue;

@Tag("API")
public class LoginTest extends BaseTest {
    private final static String URL = "https://dn-r.ru/";
    String login = "art822@rambler.ru";    String pass = "vostok1"; //Позитив

    String loginWrong = "art82@rambler.ru";    String passWrong = "vostok"; //Негатив не верно

    @Test
    @Owner("MivchenkoAV")
    @Description("дергаем автозапчасти")
    public void loginTest (){
        LoginPage loginPage = new LoginPage();
        loginPage.open(URL);
        loginPage.clickEntry();
        assertTrue(loginPage.atPage());
        loginPage.enterUserName(login);
        loginPage.enterPassword(pass);
        loginPage.clickLogin();
        HomePage homePage = new HomePage();
        homePage.loggedIn();
    }

    @Test
    @Owner("MivchenkoAV")
    @Description("дергаем автозапчасти")
    public void wrongLoginTest (){
        LoginPage loginPage = new LoginPage();
        loginPage.open(URL);
        loginPage.clickEntry();
        assertTrue(loginPage.atPage());
        loginPage.enterUserName(loginWrong);
        loginPage.enterPassword(passWrong);
        loginPage.clickLogin();
        assertTrue(loginPage.invalidNamePass());

    }
}